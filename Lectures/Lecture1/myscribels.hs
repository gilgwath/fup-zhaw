x = "hello world!"

f :: Integer -> Integer
f a = 7 * a

next x
    | mod x 2 == 0  = div x 2
    | otherwise     = 3 * x + 1

nexxt y
    | y `mod` 2 == 0    = y `div` 2
    | otherwise         = 3 * y + 1

max3 (x,y,z)
  | x >= y && x >= z = x
  | y >= x && x >= z = y
  | otherwise = z
